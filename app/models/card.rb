class Card < ActiveRecord::Base

	INITIAL_CARDS_SET = Card.all * 24

	validates_presence_of :name, :points

	def card_name
		if name.to_i == 0
			name.first
		else
			name.to_i
		end
	end
end
