class Game < ActiveRecord::Base
	has_many :steps

	validates_presence_of :player_score, :dealer_score, :player_id, :player_bet

	def self.compute_game_won_by(player_score, dealer_score)
		if player_score > 21 or dealer_score == 21
			"dealer"
		elsif player_score == 21 or dealer_score > 21
			"player"
		else
			nil
		end
	end
end		
