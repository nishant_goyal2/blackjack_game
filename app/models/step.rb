class Step < ActiveRecord::Base
	belongs_to :game
	belongs_to :card

	validates_presence_of :game_id, :card_id, :card_drawn_by
	validates :card_drawn_by, :inclusion => { :in => ['player', 'dealer']}
end
