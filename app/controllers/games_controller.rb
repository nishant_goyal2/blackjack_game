class GamesController < ApplicationController
  	
  	before_action :set_game, :except => :setup_game
    before_action :set_user_games, :except => [:setup_game]
  	before_action :game_open_check, :only => [:skip_card, :pick_a_card]
    def setup_game
		cookies[:player_id] = cookies[:player_id].present? ? cookies[:player_id] : Game.last.try(:player_id).present? ? Game.last.try(:player_id) + 1 : 1
  		@dealer_cards = [Card::INITIAL_CARDS_SET.sample]
  		@player_cards = 2.times.map { Card::INITIAL_CARDS_SET.sample }
  		
  		player_score = @player_cards.map(&:points).map{|card_point| card_point.to_i}.sum
  		dealer_score = @dealer_cards.map(&:points).map{|card_point| card_point.to_i}.sum

  		@game = Game.create(:player_score => player_score, :dealer_score => dealer_score, :player_id => cookies[:player_id], :player_bet => params[:game][:player_bet], :game_won_by => Game.compute_game_won_by(player_score, dealer_score))

  		Step.create([{:game_id => @game.id, :card_id => @dealer_cards.first.id, :card_drawn_by => 'dealer'}, 
  			{:game_id => @game.id, :card_id => @player_cards.first.id, :card_drawn_by => 'player'}, 
  			{:game_id => @game.id, :card_id => @player_cards.last.id, :card_drawn_by => 'player'}
  		])
        @user_games = Game.where(:player_id => @game.player_id)

  	end

  	def statistics
        @games = @user_games.last(4)
        @player_cards = Step.where(:card_drawn_by => 'player', :game_id => @games.map(&:id)).map{|step| step.card }
        @dealer_cards = Step.where(:card_drawn_by => 'dealer', :game_id => @games.map(&:id)).map{|step| step.card }
  	end

  	def skip_card
  		cards_left_in_deck = Card::INITIAL_CARDS_SET - @game.steps.map{|step| step.card } 
  		player_score = @game.player_score
  	
  		begin
  			new_card = cards_left_in_deck.sample	
  			cards_left_in_deck.delete(new_card)
  			Step.create(:game_id => @game.id, :card_id => new_card.id, :card_drawn_by => 'dealer')
  		end while Step.where(:card_drawn_by => 'dealer', :game_id => @game.id).map{|step| step.card.points.to_i}.sum <= 16

  		@player_cards = @game.steps.group_by(&:card_drawn_by)["player"].map{|dealer_step| dealer_step.card}
  		@dealer_cards = Step.where(:card_drawn_by => 'dealer', :game_id => @game.id).map{|step| step.card }
  		dealer_score = @dealer_cards.map(&:points).map{|card_point| card_point.to_i}.sum

  		game_won_by = if Game.compute_game_won_by(player_score.to_i, dealer_score).nil? && dealer_score >= 17
  			player_score.to_i > dealer_score ? "player" : "dealer"
  		else 
  			Game.compute_game_won_by(player_score.to_i, dealer_score)
  		end
  		
  		@game.update_attributes(:dealer_score => dealer_score,   :game_won_by => game_won_by)
  		render :setup_game
  	end

  	def pick_a_card
        cards_left_in_deck = Card::INITIAL_CARDS_SET - @game.steps.map{|step| step.card }
  		new_card = cards_left_in_deck.sample
  		player_score = @game.player_score.to_i + new_card.points.to_i
  		dealer_score = @game.dealer_score.to_i

  		@game.update_attributes(:player_score => player_score, :game_won_by => Game.compute_game_won_by(player_score, dealer_score))
  		Step.create(:game_id => @game.id, :card_id => new_card.id, :card_drawn_by => 'player')

  		@player_cards = Step.where(:card_drawn_by => 'player', :game_id => @game.id).map{|step| step.card }
  		@dealer_cards = Step.where(:card_drawn_by => 'dealer', :game_id => @game.id).map{|step| step.card }

  		render :setup_game
  	end

  	private
  	def set_game
  		@game = Game.find_by_id(params[:game_id])
  	end

    def set_user_games
      @user_games = Game.where(:player_id => cookies[:player_id])
    end

    def game_open_check
        if Game.compute_game_won_by(@game.player_score.try(:to_i), @game.dealer_score.try(:to_i)).present?
            redirect_to root_url, :notice => "#{@game.game_won_by.capitalize} has already won the game."
        end
    end

end


