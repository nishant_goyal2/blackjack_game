module GamesHelper

	def get_player_winnings
	 	games_won_by_player = @user_games.group_by(&:game_won_by)['player']
		games_won_by_player.present? ? games_won_by_player.map{|game| game.player_bet.to_i}.sum : "0"
	end

	def get_player_total_bet
		@user_games.map{|game| game.player_bet.to_i}.sum
	end
end
