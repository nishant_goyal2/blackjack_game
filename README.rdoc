== README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version: 2.2.1

* Basic steps to be followed in application setup
 	1. export rails env to production
 	2. rake db:create
  	3. rake db:migrate
  	4. rake db:seed
  	5. rake assets:precompile
  	6. add secret_key_base for production env in config/secrets.yml

Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.
