class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :player_score
      t.string :dealer_score
      t.string :game_won_by

      t.timestamps null: false
    end
  end
end
