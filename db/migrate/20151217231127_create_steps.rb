class CreateSteps < ActiveRecord::Migration
  def change
    create_table :steps do |t|
      t.integer :game_id
      t.integer :card_id
      t.string :card_drawn_by

      t.timestamps null: false
    end
  end
end
