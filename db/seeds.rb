# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
	cards = Card.create([
		{ name: "2", :points => "2"},
		{ name: "3", :points => "3" },
		{ name: "4", :points => "4" },
		{ name: "5", :points => "5" },
		{ name: "6", :points => "6" },
		{ name: "7", :points => "7" },
		{ name: "8", :points => "8" },
		{ name: "9", :points => "9" },
		{ name: "10", :points => "10" },
		{ name: "jack", :points => "10" },
		{ name: "queen", :points => "10" },
		{ name: "king", :points => "10" },
		{ name: "ace", :points => "11" }
		])
#   Mayor.create(name: 'Emanuel', city: cities.first)
